C++ Custom Define Examples
---

Make all binaries
```
cd src
make
```

Clean all binaries
```
cd src
make clan
```

Use binary:

```
cd src
make
./filename.bin
```
